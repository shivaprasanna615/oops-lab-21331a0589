#include <iostream>
using namespace std;

class Base {
public:
   void display1() {
      cout << "base class" << endl;
   }
};

class Class1 : virtual public Base {
public:
   void display2() {
      cout << "This is Class 1" << endl;
   }
};

class Class2 : virtual public Base {
public:
   void display3() {
      cout << "This is Class 2" << endl;
   }
};

class Class3 : public Class1, public Class2 {
};

int main() {
   Class3 obj;
   obj.display1();    
   return 0;
}
