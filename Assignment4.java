import java.text.*;

public class Assignment4 {
     public static void main(String args[]) {
    String a = "100";
    String b = "100";
      
    // Printing the hashcodes of a and b
    System.out.println("***Printing the hashcodes of a and b***");
    System.out.println("HashCode of a = " + a + ": " + a.hashCode());
    System.out.println("HashCode of b = " + b + ": " + b.hashCode());
    System.out.println();





    //Format Method
    System.out.println("***Format Method***");
    double number1 = 3.14159;
    String formattedNumber1 = String.format("%.3f", number1);
    System.out.println("Formatted Number: " + formattedNumber1);
    System.out.println();









    // Grouping Method
    System.out.println("***Grouping Method***");
    double number2 = 1234567.456;

        // Get the default number format for the default locale
        NumberFormat numberFormat = NumberFormat.getInstance();

        // Check if grouping is used
        boolean isGroupingUsed = numberFormat.isGroupingUsed();

        System.out.println("Is grouping used? " + isGroupingUsed);

        // Set grouping to false
        numberFormat.setGroupingUsed(false);

        // Format the number
        String formattedNumber2 = numberFormat.format(number2);

        System.out.println("Formatted number without grouping: " + formattedNumber2);
        System.out.println();






        // Number Format Method
        System.out.println("***Number Format Method***");
        NumberFormat n = NumberFormat.getInstance();
        double val = 2.5356563736;
        System.out.println("Value: "+val);
        String formattedVal = n.format(val);
        System.out.println("Formatted Value: "+formattedVal);
        System.out.println();






        //Get Minimum Integer digits method
        System.out.println("***Get Minimum Integer digits method***");
        NumberFormat numberFormat2 = NumberFormat.getInstance();
        int minimumIntegerDigits = numberFormat2.getMinimumIntegerDigits();
        System.out.println("Minimum Integer Digits: " + minimumIntegerDigits);
        // This code outputs 1
        System.out.println();


    }

}