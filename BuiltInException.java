import java.util.Scanner;
import java.util.InputMismatchException;
class Exception{
    public static void main(String[] args) {
        
        try{
            System.out.println("Enter your number : ");
            Scanner input = new Scanner(System.in);
            int num = input.nextInt();

            System.out.println("The square of " +num+ " is " +num*num);
    
        }    
        catch(InputMismatchException obj){
            System.out.println("String is not accepted");
        }
    }
}