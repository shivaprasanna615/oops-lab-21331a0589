class Base{
    public void display1() {
        System.out.println("Base Class");
    }
}

class Class1 extends Base {
    public void display2() {
        System.out.println("Class 1");
    }
}

class Class2 extends Base {
    public void display3() {
        System.out.println("Class 2");
    }
}

class Class3 extends Class1, Class2 {
    
}

public class Main {
    public static void main(String[] args) {
        Class3 obj = new Class3();
        obj.display1(); 
        // ...
    }
}
