import java.text.NumberFormat;

public class MinimumIntegerDigitsExample {
    public static void main(String[] args) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        int minimumIntegerDigits = numberFormat.getMinimumIntegerDigits();
        System.out.println("Minimum Integer Digits: " + minimumIntegerDigits);
    }
}
