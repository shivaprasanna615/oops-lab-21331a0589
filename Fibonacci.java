package myPack;

public class Fibonacci{
    public static int fib(int n){
        if (n<=1){
            return n;
        }
        else{
            return fib(n-1)+fib(n-2);
        }
    }

    public static void fibSeries(int n){
        int f1=0;
        int f2=1;
        int f3;
        for (int i=1;i<=n;i++){
            System.out.println(f1);
            f3=f1+f2;
            f1=f2;
            f2=f3;
            }
        }
};