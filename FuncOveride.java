class Dog{
    public void Sound(){
        System.out.println("A dog barks");
    }
}

class Cat extends Dog{
    public void Sound(){
        System.out.println("A Cat meows");
    }
}

public class FuncOveride{
    public static void main(String[] args){
        Dog obj1 = new Dog();
        Cat obj2 = new Cat();
        obj1.Sound();
        obj2.Sound();
    }
}