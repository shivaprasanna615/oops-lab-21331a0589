#include<iostream>
using namespace std;
class Student {
    string fullName;
    int rollNum;
    double semPercentage;
    string collegeName;
    int collegeCode;

    public:
    Student(){

    }
    //parameterised constructor
    Student(string fname,int roll,double spercent,string cname,int ccode) {
        fullName = fname;
        rollNum = roll;
        semPercentage = spercent;
        collegeName = cname;
        collegeCode = ccode;
    }
    //method
   void setData(string fname,int roll,double spercent,string cname,int ccode){
        fullName = fname;
        rollNum = roll;
        semPercentage = spercent;
        collegeName = cname;
        collegeCode = ccode;
    }
   void displayData()
    {
        cout<<"STUDENT DETAILS"<<endl;
        cout<<" Full Name : "<<fullName<<endl;
        cout<<" Roll No. : "<<rollNum<<endl;
        cout<<" Sem Percentage : "<<semPercentage<<endl;
        cout<<" College Name : "<<collegeName<<endl;
        cout<<" College Code : "<<collegeCode<<endl;
    }
    //Destructor
    ~Student() {
        cout<<"Deallocated"<<endl;
    }

};
int main()
{
    Student obj;
    Student s("Shiva",589,90,"mvgr",33);
    obj.setData("Shiva",589,90,"mvgr",33);
    obj.displayData();
    return 0;
}