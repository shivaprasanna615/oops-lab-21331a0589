// Abstract class
abstract class Vehicle {
    // Abstract method
    public abstract void start();
    
   
    public void stop() {
        System.out.println("Vehicle stopped.");
    }
}


class Car extends Vehicle {
    public void start() {
        System.out.println("Car started.");
    }
}


class Bike extends Vehicle {
    public void start() {
        System.out.println("Bike started.");
    }
}


public class PartialAbstract {
    public static void main(String[] args) {
        Car obj1 = new Car();
        Bike obj2 = new Bike();
        
        obj1.start(); 
        obj2.start(); 
        
        obj1.stop(); 
        obj2.stop(); 
    }
}