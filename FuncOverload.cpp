#include<iostream>
using namespace std;
class FuncOverload{
    public:
    void add(int a, int b){
        cout<<"The sum is : "<<a+b<<endl;
    }

    void add(double a, double b){
        cout<<"The sum is : "<<a+b<<endl;
    }
};

int main(){

    FuncOverload obj;
    obj.add(45,60);
    obj.add(2.567,5.763);
    return 0;
}