#include <iostream>
using namespace std;
class Car {
public:
    void start() {
        cout << "Starting the car..."<<endl;
        engineOn();
        cout << "The car is now running "<<endl;
    }
private:
    void engineOn() {
        cout << "Turning the engine on..."<<endl;
    }
};

int main() {
    Car myCar;
    myCar.start();
    return 0;
}