interface Dinner {
    void eat();
}

class Rice implements Dinner {
    public void eat() {
        System.out.println("Eat Rice for dinner ");
    }
}

class Roti implements Dinner {
    public void eat() {
        System.out.println("Eat Roti for dinner ");
    }
}

public class PureAbstract {
    public static void main(String[] args) {
        Dinner obj1 = new Rice();
        Dinner obj2 = new Roti();
        
        obj1.eat(); 
        obj2.eat(); 
    }
}