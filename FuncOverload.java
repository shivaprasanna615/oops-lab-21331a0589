class Main{
    void addNum(int a,int b){
        System.out.println(a+ " + " +b+ " = " +(a+b));
    }

    void addNum(int a, int b,int c){
        System.out.println(a+ " + " +b+ " + " +c+ " = " +(a+b+c));
    }

    void addNum(double a, double b){
        System.out.println(a+ " + " +b+ " = " +(a+b));
    }

    void addNum(double a, double b, double c){
        System.out.println(a+ " + " +b+ " + " +c+ " = " +(a+b+c));
    }
}

class FuncOverload{
    public static void main(String[] args){
        Main obj = new Main();
        obj.addNum(65,78);
        obj.addNum(62,7,90);
        obj.addNum(4.89,0.67,8.899);
        obj.addNum(37.44,89.89);
    }
}