#include<iostream>
using namespace std;
class Student {
public:
    // Constructor
    Student(string name, int roll, double percentage, string college, int code) {
        fullName = name;
        rollNum = roll;
        semPercentage = percentage;
        collegeName = college;
        collegeCode = code;
    }
    // Destructor
    ~Student() {
        cout << "Student object with roll number " << rollNum << " has been destroyed." << endl;
    }
    // display functions
    void displayDetails() {
        cout << "Name : " << fullName << endl;
        cout << "Roll Number : " << rollNum << endl;
        cout << "Semester Percentage : " << semPercentage << endl;
        cout << "College Name : " << collegeName << endl;
        cout << "College Code : " << collegeCode << endl;
    }
private:
    string fullName;
    int rollNum;
    double semPercentage;
    string collegeName;
    int collegeCode;
};

int main() {
    // Create a Student object
    Student s("Shiva", 589, 90, "MVGR college", 54321);

    // Display the details of the Student object
    s.displayDetails();

    // The destructor will be called automatically when the object goes out of scope
    return 0;
}