import java.text.*;
public class FormatMethod {
    public static void main(String[] args) {
        double number = 3.14159;
        String formattedNumber = String.format("%.3f", number);
        System.out.println("Formatted Number: " + formattedNumber);
    }
}
