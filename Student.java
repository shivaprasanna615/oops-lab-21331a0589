public class Student{
    
            String fullName;
            int rollNum;
            double semPercentage;
            String collegeName;
            int collegeCode;
    // constructor
    Student(String fname, int roll, double percentage, String cname, int ccode){
        fullName = fname;
        rollNum = roll;
        semPercentage = percentage;
        collegeName = cname;
        collegeCode = ccode;
    }

    //Display details
    void displayDetails(){
        System.out.println("name : " + fullName);
        System.out.println("Roll number : " + rollNum);
        System.out.println("sem percentage : " + semPercentage);
        System.out.println("college name : " + collegeName);
        System.out.println("college code : " + collegeCode);
    }
    // destructor
    protected void finalize(){
        System.out.println(" deallocated ");
    }



//main function
    public static void main(String[] args){
        Student obj = new Student("Shiva" , 589, 90, "MVGR", 54321);
        obj.displayDetails();
        obj = null;
        System.gc();
    }
    
}