import java.util.*;

public class MapDemo {
    public static void main(String[] args) {
        Map<String, Integer> studentScores = new HashMap<>();

        studentScores.put("David", 90);
        studentScores.put("Marshal", 85);
        studentScores.put("Dube", 95);

        int DavidScore = studentScores.get("David");
        System.out.println("Alice's score: " + DavidScore);

        studentScores.put("Dube", 100);

        studentScores.remove("Marshal");

        boolean IsDube = studentScores.containsKey("Dube");
        System.out.println("Does the map contain Dave? " + IsDube);

        System.out.println("Keys in the map:");
        for (String key : studentScores.keySet()) {
            System.out.println(key);
        }

        System.out.println("Values in the map:");
        for (int value : studentScores.values()) {
            System.out.println(value);
        }

        System.out.println("Key-value pairs in the map:");
        for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();
            System.out.println(key + " : " + value);
        }

        int mapSize = studentScores.size();
        System.out.println("Size of the map: " + mapSize);

    }
}
