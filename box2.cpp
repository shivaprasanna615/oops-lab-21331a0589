#include<iostream>
using namespace std;
class Box{
    public:
    float length;
    float width;
    float height;
    //Member function inside class
    void boxArea(){
        cout<< "Area of the box is : " << 2*(length*width + width*height + height*length)<< " Sq.units" << endl;
    }
    void boxVolume();
    friend void displayBoxDimensions(Box);
    inline void displayWelcomeMessage(){
        cout << "Hello, there!"<<endl;
    }
};

//member function outside class
void Box :: boxVolume(){
    cout << "Volume of box is :" << length*width*height<<" Cubic.units"<< endl;
    cout << "" << endl;
}

//friend function
void displayBoxDimentions(Box b)
{
    cout << "Dimensions of Box : " << endl;
    cout<< "Length of box is "<< b.length << " units"<< endl;
    cout<< "Width of box is " << b.width << " units" << endl;
    cout << "Height of box is " << b.height<< " units" << endl;
}

int main(){
    Box a;
    cout<< "Enter length of the box :";
    cin>> a.length;
    cout << "Enter width of the box :";
    cin >> a.width;
    cout << "Enter height of the box :";
    cin >> a.height;
    cout << "" << endl;
    a.boxArea();
    a.boxVolume();
    displayBoxDimentions(a);
    a.displayWelcomeMessage();
}
