#include<iostream>
using namespace std;

// base class1
class base1{
    public:
        base1(){
            cout<<"Base class 1"<<endl;
    }
};

//base class2
class base2{
    public:
    base2(){
        cout<<"Base Class 2"<<endl;
    }
};

//base class3
class base3{
    public:
    base(){
        cout<<"Base Class 3"<<endl;
    }
};

// Simple inheritance
class SimpleClass : public base1{
    public:
     SimpleClass(){
        cout<<"Simple class Inherits base class 1"<<endl;
     }
};

//Multilevel inheritance
class MultilevelClass : public SimpleClass{
    public:
        MultilevelClass(){
            cout<<"Multilevel class inherits Simple class"<<endl;

        }
};

//Multiple inheritance
class MultipleClass : public base1, public base2{
    public:
        MultipleClass(){
            cout<<"Multiple class inherits both classes Base1 and Base2"<<endl;
        }
};

//Hierarchical inheritance(Hierarchical1 inherits base2)
class Hierarchical1 : public base2{
    public:
        Hierarchical1(){
            cout<<"Hierarchical Class1 inherits base class 2"<<endl;
        }
};

// Hierarchical inheritance(Hierarchical2 inherits base2)
class Hierarchical2 : public base2{
    public:
        Hierarchical2(){
            cout<<"Hierarchical class 2 inherits base class 2"<<endl;
        }
};

int main()
{
    cout << "Simple Inheritance" << endl;
    SimpleClass simple;

    cout << endl;

    cout << "Multilevel Inheritance" << endl;
    MultilevelClass multilevel;

    cout << endl;
    cout << "Multiple Inheritance" << endl;
    MultipleClass multiple;

    cout << endl;

    cout << "Hierarchical Inheritance" << endl;
    Hierarchical1 obj1;
    Hierarchical2 obj2;
}

