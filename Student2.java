class Student {
    String fullName;
    double semPercentage;
    String collegeName;
    int collegeCode;

    // default constructor
    Student(){
        collegeName = "MVGR";
        collegeCode = 33;
        System.out.println("Default Constructor Called");
        System.out.println("College Name : " + collegeName);
        System.out.println("College Code : " + collegeCode);
    }

    // Parameterized Constructor
    Student(String fname, double percentage){
        fullName = fname;
        semPercentage = percentage;
        System.out.println("Parameterized Constructor Called");
        System.out.println("Name : " + fullName);
        System.out.println("Sem Percentage : " + semPercentage);
    }

    protected void finalize(){
        System.out.println("Deallocated");
    }

    //main function
    public static void main(String[] args){
        Student obj1 = new Student();
        Student obj2 = new Student("Shiva", 90);
        obj1.finalize();
        obj2.finalize();
        
    }
}