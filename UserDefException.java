import java.util.*;
class MinorAgeException extends Exception {
    MinorAgeException(String msg) {
        super(msg);
    } 
}

class MyExcept{
    void VoterIDApplication(int n) throws MinorAgeException{
        if(n<=18) { 
            throw new MinorAgeException("Unable To apply for voter ID,Your age is less than 18");
        }else{
            System.out.println("Voter ID applied Succesfully");
        }
    }
	public static void main(String[] args) throws MinorAgeException {
	    MyExcept obj = new MyExcept();
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter your Age:");
	    int CandidateAge = sc.nextInt();
        obj.VoterIDApplication(CandidateAge);
	}
}