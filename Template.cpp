#include<iostream>
using namespace std;
template<class result>
class MyTemplate{
    int res=0;

   public:

    template<typename myDatatype1>
    void add(myDatatype1 a, myDatatype1 b){

       myDatatype1 res;

       res=a+b;

       cout<<"Result of "<<a<<" + "<<b<<" is : "<<res<<endl;

   }

};

int main()

{

   MyTemplate <int>obj;

   obj.add<int>(10,20);
   obj.add<float>(2.464,6.459);
}