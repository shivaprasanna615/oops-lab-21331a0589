import java.text.NumberFormat;
import java.util.Locale;

public class Grouping {
    public static void main(String[] args) {
        double number = 1234567.456;

        // Get the default number format for the default locale
        NumberFormat numberFormat = NumberFormat.getInstance();

        // Check if grouping is used
        boolean isGroupingUsed = numberFormat.isGroupingUsed();

        System.out.println("Is grouping used? " + isGroupingUsed);

        // Set grouping to false
        numberFormat.setGroupingUsed(false);

        // Format the number
        String formattedNumber = numberFormat.format(number);

        System.out.println("Formatted number without grouping: " + formattedNumber);
    }
}
