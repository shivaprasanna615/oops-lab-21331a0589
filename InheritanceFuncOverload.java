class Parent {
    public void addNum(int a, int b) {
        System.out.println(a+ "+" +b+ "=" + (a + b));
    }

    public void addNum(int a, int b, int c) {
        System.out.println(a+ "+" +b+ "+" +c+ " = " + (a + b + c));
    }
}

class Child extends Parent {
    public void addNum(double a, double b) {
        System.out.println(a+ "+" +b+ " = " + (a + b));
    }

    public void addNum(double a, double b, double c) {
        System.out.println(a+ "+" +b+ "+" +c+ " = " + (a + b + c));
    }
}

class InheritanceFuncOverload {
    public static void main(String[] args) {
        Child obj = new Child();
        obj.addNum(75, 331);
        obj.addNum(87.56, 77.08);
        obj.addNum(66.129, 35.64, 69.24);
    }
}
