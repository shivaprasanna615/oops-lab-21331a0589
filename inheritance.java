class Parent{
        String Height = "Tall height";
    

        String Eye_colour = "Blue eyes";
    
}

class Child1 extends Parent{
    void display1(){
        System.out.println("The first Child inherited the trait " + Height + " from his/her Parent");
    }
}

class Child2 extends Parent{
     void display2(){
        System.out.println("The second Child inherited the trait " + Eye_colour + " From his/her Parent");
    }
}

public class inheritance{
    public static void main(String[] args){
        Child1 obj1 = new Child1();
        Child2 obj2 = new Child2();
        obj1.display1();
        obj2.display2();
        
    }
}