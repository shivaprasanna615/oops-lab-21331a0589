// base class
class Base{
    Base(){
        System.out.println("Base class");
    }
}

// Simple inheritance
class SimpleClass extends Base{
    SimpleClass(){
        System.out.println("SimpleClass inherits Base Class");
    }
}

// Multilevel inheritance
class MultilevelClass extends SimpleClass{
    MultilevelClass(){
        System.out.println("MultilevelClass inherits SimpleClass");
    }
}

// Multiple Inheritance
// NOT POSSIBLE IN JAVA

// Hierarchical Inheritance(Hierarchical1 inherits Base)
class Hierarchical1 extends Base {
    Hierarchical1(){
        System.out.println("Hierarchical1 inherits Base Class");
    }
}

// Hierarchical Inheritance(Hierarchical2 inherits Base)
class Hierarchical2 extends Base {
    Hierarchical2(){
        System.out.println("Hierarchical2 inherits Base Class");
    }
}
public class inheritanceTypes{
    public static void main(String[] args){
        System.out.println("|| Simple Inheritance ||");
        SimpleClass simple = new SimpleClass();
        System.out.println();

        System.out.println("|| Multilevel Inheritance ||");
        MultilevelClass multilevel = new MultilevelClass();
        System.out.println();

        System.out.println("|| Multiple Inheritance ||");
        System.out.println("NOT POSSIBLE IN JAVA");
        System.out.println();

        System.out.println("|| Hierarchical Inheritance ||");
        Hierarchical1 h1 = new Hierarchical1();
        Hierarchical2 h2 = new Hierarchical2();

    }
}