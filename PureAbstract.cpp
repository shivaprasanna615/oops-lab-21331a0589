#include<iostream>
using namespace std;
class Photo{
    public:
        virtual void Capture()=0;
        
};

class NikonCamera : public Photo {
    public :
    void Capture(){
        cout<< " Nikon Camera captures a Photo "<<endl;
    }
};

class SamsungCamera : public Photo {
    public:
    void Capture(){
        cout<<" Samsung Camera captures a Photo "<<endl;
    }
};

int main(){
    NikonCamera obj1;
    obj1.Capture();
    SamsungCamera obj2;
    obj2.Capture();
}