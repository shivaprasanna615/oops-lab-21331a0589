import myPack.Fibonacci;
import java.util.*;
public class Mypro {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the n value :");
        int n = s.nextInt();
        System.out.print("The "+ n + "th fibonacci number is : ");
        System.out.println(Fibonacci.fib(n));
        System.out.println("The fibonacci series upto "+ n +" terms :");
        Fibonacci.fibSeries(n);
    }
};