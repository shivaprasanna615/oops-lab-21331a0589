import java.text.NumberFormat;
public class NumberFormatMethod {
    public static void main(String args[]) {
       NumberFormat n = NumberFormat.getInstance();
       double val = 2.5356563736;
       System.out.println("Value: "+val);
       String formattedVal = n.format(val);
       System.out.println("Formatted Value: "+formattedVal);
    }
}