import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.*;
public class EventRegistrationForm extends Frame {
    private Label nameLabel, ageLabel, genderLabel, dobLabel, emailLabel, phoneLabel, SuccessLabel;
    private TextField nameField, ageField, dobField, emailField, phoneField;
    private Choice genderChoice;
    private Button submitButton;

    public EventRegistrationForm() {
        setLayout(new FlowLayout());

        nameLabel = new Label("Name: ");
        add(nameLabel);

        nameField = new TextField(20);
        add(nameField);

        ageLabel = new Label("Age: ");
        add(ageLabel);

        ageField = new TextField(5);
        add(ageField);

        genderLabel = new Label("Gender: ");
        add(genderLabel);

        genderChoice = new Choice();
        genderChoice.add("Male");
        genderChoice.add("Female");
        genderChoice.add("Other");
        add(genderChoice);

        dobLabel = new Label("Date of Birth: ");
        add(dobLabel);

        dobField = new TextField(10);
        add(dobField);

        emailLabel = new Label("Email: ");
        add(emailLabel);

        emailField = new TextField(20);
        add(emailField);

        phoneLabel = new Label("Phone: ");
        add(phoneLabel);

        phoneField = new TextField(10);
        add(phoneField);

        submitButton = new Button("Submit");
        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               SuccessLabel = new Label("Registration Successful");
               add(SuccessLabel);


                String name = nameField.getText();
                String age = ageField.getText();
                String gender = genderChoice.getSelectedItem();
                String dob = dobField.getText();
                String email = emailField.getText();
                String phone = phoneField.getText();

                //Printing the details in console
                System.out.println("Name: " + name);
                System.out.println("Age: " + age);
                System.out.println("Gender: " + gender);
                System.out.println("Date of Birth: " + dob);
                System.out.println("Email: " + email);
                System.out.println("Phone: " + phone);

                
                
            }
        });
        add(submitButton);

         addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                dispose();
                System.exit(0);
            }
        });

        setTitle("Event Registration Form");
        setSize(300, 250);
        setVisible(true);
    }

    public static void main(String[] args) {
        new EventRegistrationForm();
    }
}
